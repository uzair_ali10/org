---
title: FAQ ❓
description: 'Frequently asked questions about Grey software'
position: 4
category: Info
---

### How does the organization plan to sustain itself?

Please read our [current business plan brainstorming](https://hackmd.io/@grey-software/Business)

### Are you focussing on all open source software or a particular set of open source software. 

Open source software is a broad category. We are currently focussing on modern web application development because we feel that empowering students with the ability to build open source software on the web allows them access to a powerful tool set with which they can deploy full stack applications

### Do students want something like this to exist 

There are many Computer science students around the world who want to participate in the tech economy by building a startup or getting hired as a software engineer. 

They want:

- To become better software engineers by building out their app ideas.
- To build a better resume and digital portfolio to better their employment prospects. 

Currently, they are building projects alone without mentorship or a structured pathway to implementation that uses tools and processes from the industry.       

Some of the most common problems they face are:

**Not having a senior engineer to give technical guidance** 

“Without having the assistance whenever we need it, we had to spend hours googling and asking online.” - Baichen

“Even though tons of youtube videos are available, it’s a different experience to have someone that understands what you are going through to guide you in the project.” - Brian

**No structured team environment**

“When you work in a team and it’s easier to have the motivation because you see the project moving forward and everyone doing their part.” - Lee

**No clear roadmap to implementation**

“There was no clear leader, everyone was equal, but thought their idea was superior. So there was a lot of back and forth, instead of meaningful work.” - Baichen

“It’s easy to lose sight of your goals and lose track of progress” - Billy 

“As an individual project, you are required to know every aspect of the component including frontend, backend, etc. As a result of that, for most students they have a significant knowledge gap to fill. This preparation process can be time-consuming and discouraging.”  - Brian

**No professional best practices**

“We had no knowledge of the professional workflow. We didn't set up a git repo, and accidentally lost some source code.” - Baichen 

## Does open source software kill competition?

Open sourcing your software doesn’t prevent competitors from creating software similar in functionality but different in implementation.

## How do I join Grey Software?

You can come join our [Discord Community](http://community.grey.software/) where you can discuss How you can fit in with our open source software development plans. 

We also have an [onboarding website](https://onboarding.grey.software/) that is currently under progress. 

## Is Grey Software only for Software Engineers?

Not at all!

Software engineers write the code behind Grey Software’s apps, but designers,
product owners, and researchers are integral to our software development
process.

## How can I volunteer for Grey Software?

Join community.grey.software and select the volunteer role!

You can then schedule a meeting using our [booking portal](http//meet.grey.software)

## Why is Grey Software a not-for-profit organization?

Grey Software was founded around a mission to build the software ecosystem of
the future.

To ensure that the organization’s future revenues went towards our mission
instead of the pockets of shareholders, we founded the organization as a
not-for-profit.

## What is Grey Software’s vision?

Grey Software envisions an open source future where mentors and students build
free software together.

## How are Grey Software’s products connected to its students?

Our students learn software development by collaborating with the creators of
the products they're working on.

## How is Grey Software different from other educational sites, such as Udemy, Udacity, and Coursera?

These websites host educational content that many rely on, including Grey
Software's mentors and students.

Our mentors teach by collaborating with individuals or small groups on
real-world projects using the same open-source technologies that are used in the
industry.

## How does Grey Software deliver its educational content?

Grey Software hosts students who collaborate with mentors to build our open
-source products in a real-world software development environment with mentors.

Since we create our apps and teach students how to develop them simultaneously,
it’s hard to define a structured curriculum and predict learning outcomes.

We therefore propose that each project have creation teams and education teams.

The creation team evolves our products using the latest, most productive
technologies and trends in software development.

The education team turns our products into projects that students can learn
from. This team abstracts away certain software complexities, allowing students
to gradually make their way to working with production code.

Mentors collaborate with apprentices live over video call and VS Code Live
Share.

## Are Grey Software’s apps free?

Grey Software develops software that protects human freedoms. While most of our
apps are free, we may still offer paid software in the future. Free software
refers to 'freedom', not necessarily 'gratis'.
 