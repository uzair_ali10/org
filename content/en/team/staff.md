---
title: Staff 💼
description: Grey Software's Team
category: Team
position: 9
staff:
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    position: President
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash
  - name: Raj
    avatar: https://gitlab.com/uploads/-/system/user/avatar/8089604/avatar.png
    position: Designer
    github: https://github.com/teccUI
    gitlab: https://gitlab.com/teccUI
    linkedin: https://www.linkedin.com/in/raj-paul-368827136/
  - name: Hamees
    avatar: https://assets.gitlab-static.net/uploads/-/system/user/avatar/8058458/avatar.png
    position: Administrative Officer
    github: https://github.com/mhamees
    gitlab: https://gitlab.com/mhamees
    linkedin: https://www.linkedin.com/in/muhammad-hamees/
  - name: Adil
    avatar: https://gitlab.com/uploads/-/system/user/avatar/7507821/avatar.png?width=400
    position: Software Developer
    github: https://github.com/adilshehzad
    gitlab: https://gitlab.com/adilshehzad
    linkedin: https://www.linkedin.com/in/adilshehzad7/
---

<team-profiles :profiles="staff"></team-profiles>
